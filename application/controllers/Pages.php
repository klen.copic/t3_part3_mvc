<?php

//https://www.studenti.famnit.upr.si/~klen/CI_121021/index.php/Pages/view/about

class Pages extends CI_Controller {

	public function view($page = 'home')
	{
		$this->load->helper('url_helper');
                

		$data["title"] = $page;
		$this->load->view('templates/header',$data);
		$this->load->view('pages/'.$page);
		$this->load->view('templates/footer');
	}
}
